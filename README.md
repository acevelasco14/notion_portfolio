# Ace Gabriel M. Velasco

I am a Flutter Trainee in FFUF Manila which is based in Makati city in Manila with 7 years of coding experience in web, software and mobile development.

My focus area has always been full-stack web development but now decided to shift to mobile development. I have experience using the XAMP stack and some experience using the MERN stack. I love working with teams and enjoy learning new things in programming with my colleagues in the field of software development.

# Contact Information

LinkedIn: [https://www.linkedin.com/in/ace-gabriel-velasco-0814/](https://www.linkedin.com/in/ace-gabriel-velasco-0814/)

Website Portfolio: [https://velasco-flutter-portfolio.herokuapp.com/#/](https://velasco-flutter-portfolio.herokuapp.com/#/)

# Work Experience

## Programmer Intern

Anteriore Solutions, from February 2021 to May 2021

- I developed and converted an old website to Next.js.
- I developed and converted an admin page to React.js.
- I developed a Login/Authentication page for an Admin Page.

## Flutter Trainee

FFUF Manila, from July 2021 to August 2021

- I was trained to make mobile softwares using the Flutter language.
- I refreshed my knowledge in JS-OOP.

# Skills

## Technical Skills

- Dart
- Flutter
- Software Development
- Web Development
- Mobile Development
- Git

## Soft Skills

- Teamwork
- Problem Solving
- Adaptability
- Dependability

# Education

## BS Information Technology

University of the east 2014 - 2016

Our Lady of Fatima University 2017 - 2021